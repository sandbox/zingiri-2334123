<?php
/**
 * Class containing common Bookings functions, used for Drupal 6 and 7.
 */
class BookingsCoreCommon {

	/**
	 * Checks if the module has already been configured.
	 */
	function active() {
		if ($this->get_option('zingiri_bookings_key')) return true;
		else return false;
	}
	
	/**
	 * Creates a unqiue API key.
	 *
	 * @return string API key
	 */
	function createApiKey() {
		$key='';
		$uid=uniqid($this->siteUrl(), false);
		$data=serialize($_SERVER);
		$hash=strtoupper(hash('ripemd128', $uid . $key . md5($data)));
		$key=substr($hash, 0, 8) . '-' . substr($hash, 8, 4) . '-' . substr($hash, 12, 4) . '-' . substr($hash, 16, 4) . '-' . substr($hash, 20, 12);
		return $key;
	}

	/**
	 * Retrieves the site URL.
	 *
	 * @return string Site URL
	 */
	function siteUrl() {
		global $base_url;
		return $base_url;
	}

	/**
	 * Retrieves front end page URL.
	 *
	 * @return unknown
	 */
	function home() {
		global $base_url;
		return $base_url . '/' . $this->current_path() . '?';
	}

	/**
	 * Gets the Javascript sub directory location.
	 *
	 * @return string Sub directory
	 */
	function jsPrefix() {
		if (defined('BOOKINGS_JSPREFIX')) return BOOKINGS_JSPREFIX;
		else return 'min';
	}

	/**
	 * Retrieve URL end point for web services stored on Zingiri servers.
	 *
	 * @return string URL
	 */
	function endPointUrl($endpoint=true) {
		if (defined('BOOKINGS_ENDPOINT')) $url=BOOKINGS_ENDPOINT;
		elseif (variable_get('zingiri_bookings_region','') == 'us1') $url='http://bookings4us1.zingiri.net/us1/';
		elseif (variable_get('zingiri_bookings_region','') == 'eu1') $url='http://bookings4us1.zingiri.net/eu1/';
		if ($endpoint) $url.='api.php';
		return $url;
	}

	/**
	 * Creates the HTTP request URI.
	 *
	 * @param string $page Page to retrieve
	 * @param mixed $getVars GET variables
	 * @return string URI
	 */
	function http($page="index", $getVars=array()) {
		global $user, $post, $bookings_shortcode_id;
		
		$vars="";
		$http=$this->endPointUrl();
		$and='?';
		if (!isset($_REQUEST['zfaces'])) {
			$http.='?zfaces=' . $page;
			$and="&";
		}
		if (count($_GET) > 0) {
			foreach ($_GET as $n => $v) {
				if (!in_array($n, array('page','bookingsasid','q'))) {
					if (is_array($v)) {
						foreach ($v as $w) {
							$vars.=$and . $n . '[]=' . urlencode($w);
							$and="&";
						}
					} else {
						$vars.=$and . $n . '=' . urlencode($v);
						$and="&";
					}
				}
			}
		}
		
		if (count($getVars) > 0) {
			foreach ($getVars as $n => $v) {
				$vars.=$and . $n . '=' . urlencode($v);
				$and="&";
			}
		}
		$and="&";
		
		$wp=array();
		if ($this->is_user_logged_in()) {
			$wp['login']=$user->name;
			$wp['email']=$user->mail;
			$wp['first_name']=ucfirst($user->name);
			$wp['last_name']='';
			$wp['roles']=$user->roles;
		}
		$wp['lic']=$this->get_option('zingiri_bookings_lic');
		$wp['gmt_offset']=$this->get_option('gmt_offset');
		$wp['siteurl']=$this->get_option('zingiri_bookings_siteurl') ? $this->get_option('zingiri_bookings_siteurl') : $this->home();
		$wp['sitename']=$this->get_bloginfo('name');
		$wp['pluginurl']=$this->pluginUrl();
		$wp['client']='drupal';
		if (!$this->is_admin()) {
			$wp['mode']='f';
			$wp['pageurl']=$this->home();
			if (isset($post)) $wp['sid']=$post->ID . '-' . (isset($bookings_shortcode_id) ? $bookings_shortcode_id : '1');
		} else {
			$wp['mode']='b';
			$wp['pageurl']=$this->get_admin_url();
			$wp['secret']=$this->get_option('zingiri_bookings_secret');
		}
		if ($this->get_option('zingiri_bookings_showcase')) {
			$wp['desc']=$this->get_bloginfo('description');
			$wp['showcase']=1;
		}
		$wp['time_format']=$this->get_option('time_format');
		$wp['admin_email']=$this->get_option('admin_email');
		$wp['key']=$this->get_option('zingiri_bookings_key');
		$wp['lang']=$this->get_bloginfo('language');
		$wp['client_version']=$this->version();
		if (isset($_SESSION['bookings']['force_license_check'])) {
			$wp['force_license_check']=true;
			unset($_SESSION['bookings']['force_license_check']);
		}
		
		$vars.=$and . 'wp=' . urlencode(base64_encode(json_encode($wp))) . '&aphps_dev='.(defined('APHPS_DEV') ? APHPS_DEV : 0);
		
		$_SESSION['bookings']['wp']=urlencode(base64_encode(json_encode($wp)));
		
		if ($this->get_option('zingiri_bookings_http_referer')) $vars.='&http_referer=' . urlencode($this->get_option('zingiri_bookings_http_referer'));
		if ($vars) $http.=$vars;
		
		return $http;
	}

	/**
	 * Retrieves page content.
	 *
	 *
	 * Builds the request URI and calls the URI to retrieve the page content.
	 *
	 * @param string $bookings_to_include Page to retrieve
	 * @param mixed $postVars POST variables
	 * @param mixed $getVars GET variables
	 * @return string|boolean False or string if an error occured
	 */
	function output($bookings_to_include='', $postVars=array(), $getVars=array()) {
		global $post, $bookings, $bookingsTemplate;
		global $wpdb;
		global $wordpressPageName;
		global $bookings_loaded;
		
		$ajax=isset($_REQUEST['ajax']) ? $_REQUEST['ajax'] : false;
		
		$http=$this->http($bookings_to_include, $getVars);
		$this->log('Notification', 'Call: ' . $http);
		$news=new bookingsHttpRequest($http, 'bookings');
		$news->noErrors=true;
		$news->post=array_merge($news->post, $postVars);
		
		if (!$news->curlInstalled()) {
			$this->log('Error', 'CURL not installed');
			return "cURL not installed";
		} elseif (!$news->live()) {
			$this->log('Error', 'A HTTP Error occured');
			return "A HTTP Error occured";
		} else {
			if (($ajax == 1) && !in_array($_REQUEST['form'], array('form_field','form'))) {
				while ( count(ob_get_status(true)) > 0 )
					ob_end_clean();
				$buffer=$news->DownloadToString();
				$bookings['output']=json_decode($buffer, true);
				if (!$bookings['output']) {
					$bookings['output']['body']=$buffer;
					$bookings['output']['head']='';
				}
				if (isset($_REQUEST['scr'])) {
					echo $bookings['output']['body'];
				} elseif ($ajax == '1') {
					echo $bookings['output']['body'];
				} else {
					echo '<html><head>';
					echo $bookings['output']['head'];
					echo '</head><body>';
					echo $bookings['output']['body'];
					echo '</body></html>';
				}
				die();
			} elseif (($ajax == 1) && in_array($_REQUEST['form'], array('form_field','form'))) {
				if (!defined('BOOKINGS_AJAX_ORIGIN')) while ( count(ob_get_status(true)) > 0 )
					ob_end_clean();
				$buffer=$news->DownloadToString();
				$output=json_decode($buffer, true);
				echo $output['body'];
				if (!defined('BOOKINGS_AJAX_ORIGIN')) die();
			} elseif (($ajax == 2) || (($ajax == 1) && ($_REQUEST['form'] == 'form_field'))) {
				while ( count(ob_get_status(true)) > 0 )
					ob_end_clean();
				$output=$news->DownloadToString();
				foreach (array('content-disposition','content-type') as $i) {
					if (isset($news->headers[$i])) header($i . ':' . $news->headers[$i]);
				}
				if (isset($news->body)) echo $news->body;
				die();
			} elseif ($ajax == 3) {
				while ( count(ob_get_status(true)) > 0 )
					ob_end_clean();
				$buffer=$news->DownloadToString();
				$output=json_decode($buffer, true);
				echo $output['body'];
				die();
			} else {
				$buffer=$news->DownloadToString();
				if ($news->error) {
					$bookings['output']=array();
					if ($this->is_admin()) $bookings['output']['body']='An error occured when connecting to the Zingiri Bookings service.<br />If you need help with this, please contact our <a href="http://www.zingiri.com/go" target="_blank">technical support service</a>.';
					else $bookings['output']['body']='The service is currently not available, please try again later.';
					return false;
				}
				$bookings['output']=json_decode($buffer, true);
				if (isset($bookings['output']['reload']) && $bookings['output']['reload']) {
					$buffer=$news->DownloadToString();
					$bookings['output']=json_decode($buffer, true);
				}
				if (!$bookings['output']) {
					$bookings['output']['body']=$buffer;
					$bookings['output']['head']='';
				} else {
					if (isset($bookings['output']['http_referer'])) $this->update_option('zingiri_bookings_http_referer', $bookings['output']['http_referer']);
					else $this->update_option('zingiri_bookings_http_referer', '');
				}
				if (isset($bookings['output']['template']) && $bookings['output']['template']) $bookingsTemplate=$bookings['output']['template'];
				else $bookings['output']['template']=$bookingsTemplate;
				// $bookings['output']['body']=bookings_parser($bookings['output']['body']);
			}
		}
	}

	/**
	 * Checks if the user is logged in.
	 *
	 * @return boolean True if logged in, false otherwise
	 */
	function is_user_logged_in() {
		global $user;
		if (isset($user->uid) && $user->uid) return true;
		else return false;
	}

	/**
	 * Retrieves an option (aka variable, setting) value.
	 *
	 * @param string $option Name
	 */
	function get_option($option) {
		switch ($option) {
			case 'zingiri_bookings_siteurl' :
				return $this->siteUrl();
				break;
			case 'admin_email' :
				return variable_get('site_mail','');
				break;
			default :
				return variable_get($option,'');
				break;
		}
	}

	/**
	 * Updates an option (aka variable, setting).
	 *
	 * @param string $option Name
	 * @param string $value Value
	 */
	function update_option($option, $value) {
		variable_set($option, $value);
	}

	/**
	 * Retrieves info about the site.
	 *
	 * @param string $option Name
	 * @return string Value
	 */
	function get_bloginfo($option) {
		global $language;
		switch ($option) {
			case 'name' :
				return variable_get('site_name','');
				break;
			case 'description' :
				return variable_get('site_slogan','');
				break;
			case 'language' :
				if ($language->language == 'en') return 'en_US';
				else return $language->language;
				break;
		}
	}

	/**
	 * Retrieves the plugin URL.
	 * This is used for example to build the full path to the CSS files.
	 *
	 * @return string URL
	 */
	function pluginUrl() {
		global $base_url;
		return $base_url . '/' . drupal_get_path('module', 'zingiri_bookings') . '/';
	}

	/**
	 * Check if current page is an administration page.
	 *
	 * @return boolean True if it is an administration page, false otherwise
	 */
	function is_admin() {
		if (user_access('administer modules') && $this->path_is_admin($this->current_path())) return true;
		else return false;
	}

	/**
	 * Retrieves the URL of the administration page.
	 *
	 * This is basically the path of the current page when viewing an admin page.
	 *
	 * @return string URL
	 */
	function get_admin_url() {
		global $base_url;
		return $base_url . '/' . $this->current_path() . '?';
	}

	/**
	 * Log debug messages.
	 * (Not implemented)
	 *
	 * @param string $type Message type, e.g. error, warning, etc
	 * @param string $message Message content
	 */
	function log($type, $message) {
	}

	/**
	 * Retrieves URL of the Ajax page.
	 *
	 * @param string $admin Set to true if requesting the URL of admin Ajax page, false if requesting the URL of the
	 *        front end Ajax page
	 * @return string URL
	 */
	function ajaxUrl($admin=false) {
		global $base_url;
		if ($admin) return $base_url . '/admin/config/content/zingiri/bookings/ajax';
		else return $base_url . '/zingiri/bookings/ajax';
	}

	/**
	 * Retrieve the version of the module.
	 * 
	 * @return string Version number
	 */
	function version() {
		$path=drupal_get_path('module', 'zingiri_bookings') . '/' . 'zingiri_bookings' . '.info';
		$info=drupal_parse_info_file($path);
		return $info['version'];
	}
	
	function cdn($sub='') {
		if (defined('BOOKINGS_CDN')) return BOOKINGS_CDN;
		else return 'http://cdn.zingiri.net/bookings/' . ($sub ? $sub . '/' : '');
	}
}