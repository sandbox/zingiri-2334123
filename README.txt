CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* Troubleshooting
* Design Decisions

INTRODUCTION
------------
Zingiri Bookings is a hosted online booking and reservation solution. 

The Zingiri Bookings for Drupal plugin allows to integrate the Zingiri Bookings service in Drupal, providing both access for administrators to manage the system and for customer and users to make reservations. 

REQUIREMENTS
------------
This module requires the following modules:
* jQuery Update (https://www.drupal.org/project/jquery_update)

INSTALLATION
------------
* Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
   
CONFIGURATION
-------------
* Go to the Modules » Other and click on the Configure link
  - The API Key is automatically generated. Together with your website URL it is used to create on the Zingiri servers. 
  - An optional License Key can be purchased. This will unlock additional features. A full description can be found on http://www.zingiri.com/bookings
  - Choose the Region that is closest to where you are located.
  After clicking on 'Save configuration' you will see a number of additional tabbed menus appearing. These menus allow you to create schedules, resources, products & services and start taking reservations online. 
* Configure user permissions via Modules » Other » Bookings » Permissions:
  - Access front-end page
    You can allow Anonynous and Authenticated Users access to the front end
  - Access administration pages
    You'll want to restrict access to the back end administration pages to Administrators only 
* To access the front end bookings page, use the URL http(s)://mysite/bookings.     

TROUBLESHOOTING
---------------
* If after having configured the module, the additional tabbed menus are not appearing, try clearing the Drupal caches.

DESIGN DECISIONS
----------------
We are trying to use as much as possible of the same code base for Drupal 6 and Drupal 7. Things that are specific to either version can be found in bookings_core.inc. 
Sometimes this means that the code might look a bit superfluous, like defining the method BookingsCore::current_path() that in Drupal 7 simply returns current_path().
However the code in Drupal 6 is quite a bit different since the current_path() doesn't exist in Drupal 6.