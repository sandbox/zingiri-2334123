<?php
module_load_include('inc', 'zingiri_bookings', 'zingiri_bookings_core_common');

/**
 * Class containing functions specific to Drupal 7.
 */
class BookingsCore extends BookingsCoreCommon {

	function current_path() {
		return current_path();
	}

	function path_is_admin($path) {
		return path_is_admin($path);
	}

	/**
	 * Load required jQuery UI libraries
	 */
	function addjQueryUi($libs) {
		foreach ($libs as $lib) {
			drupal_add_library('system', $lib);
		}
	}

	/**
	 * Load Javascripts from the server.
	 * We use the native Drupal function for this.
	 */
	function addExternalJs($js) {
		foreach ($js as $external_js) {
			drupal_add_js($external_js, 'external');
		}
	}

	/**
	 * Load CSS from the server.
	 * We use the native Drupal function for this.
	 */
	function addExternalCss($css) {
		foreach ($css as $external_css) {
			drupal_add_css($external_css, 'external');
		}
	}
}